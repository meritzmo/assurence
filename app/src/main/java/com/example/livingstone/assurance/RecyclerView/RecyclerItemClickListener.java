package com.example.livingstone.assurance.RecyclerView;

import android.view.View;

/**
 * Created by livingstone on 13/02/18.
 */

public interface RecyclerItemClickListener {
    public void onClick(View v, int position);
    public void onLongClick(View v, int position);

}

