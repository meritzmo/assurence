package com.example.livingstone.assurance.monCompte;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.livingstone.assurance.Polices;
import com.example.livingstone.assurance.R;
import com.example.livingstone.assurance.RecyclerView.CustomRecyclerViewItemClickListener;
import com.example.livingstone.assurance.RecyclerView.PolicesRecyclerAdapter;
import com.example.livingstone.assurance.RecyclerView.RecyclerItemClickListener;
import com.example.livingstone.assurance.WebService;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;


public class MesPolicesActivity extends AppCompatActivity implements WebService.WebServiceListener{


    ProgressDialog progress;
    RecyclerView recyclerView;
    TextView impaye;
    Dialog boxDetails;

    List<Polices> polices_ws = new ArrayList<>();

    List<PolicesRecyclerAdapter.mesPolices> data_polices = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mon_recycler_view);


        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progress = new ProgressDialog(this);

        AjoutPoliceTask policeTask = new AjoutPoliceTask();
        policeTask.execute();

        recyclerView.addOnItemTouchListener(new CustomRecyclerViewItemClickListener(this, new RecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                TextView num=(TextView)v.findViewById(R.id.No_pol);

                boxDetails = new Dialog(MesPolicesActivity.this);
                boxDetails.setContentView(R.layout.details_polices);
                boxDetails.setTitle(num.getText().toString());
                boxDetails.show();
            }

            @Override
            public void onLongClick(View v, int position) {
            }
        }));


    }

    public void dismissPolice(View view) {
        boxDetails.dismiss();
    }


    class AjoutPoliceTask extends AsyncTask<Void, Void, Void>{

        Boolean network;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setMessage("Connexion... ");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
        }


        @Override
        protected Void doInBackground(Void... voids) {

            WebService ws = new WebService(MesPolicesActivity.this);
            String data=ws.getPolice();



            if(!data.equals("no_network"))
            {
                network =true;

                polices_ws = WebService.parse(data);

                if ((polices_ws!=null) && (polices_ws.size() != 0) ) {

                    for (Polices t :polices_ws) {
                        data_polices.add(new PolicesRecyclerAdapter.mesPolices(
                                t.getNum_police(),
                                t.getNomProduit(),
                                t.getCapital(),
                                t.getPrime(),
                                t.getPeriodicite(),
                                t.getValeurPm(),
                                t.getImpaye()
                        ));

                       // impaye.setText(t.getImpaye());

                    }

                }
            }else {
                    network =false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // mAuthTask = null;

            if(network){
                //mPgBar.setVisibility(View.GONE);
                progress.cancel();
                recyclerView.setAdapter(new PolicesRecyclerAdapter (data_polices));
            } else {
                //mPgBar.setVisibility(View.GONE);
                progress.cancel();
                afficher("Problème de connexion au serveur");
            }

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void afficher(String err) {
        android.support.v7.app.AlertDialog.Builder ad= new android.support.v7.app.AlertDialog.Builder(this);
        ad.setTitle("Erreur");
        ad.setMessage(err);
        ad.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ad.show();

    }
}
