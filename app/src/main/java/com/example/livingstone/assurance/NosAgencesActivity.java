package com.example.livingstone.assurance;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class NosAgencesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nos_agences);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    public void agencesLome(View view) {

        Dialog boxLome= new Dialog(this);
        boxLome.setContentView(R.layout.agences_lome);
        boxLome.setTitle("Agences de Lomé");
        boxLome.show();



    }

    public void agencesInterieur(View view) {

        Dialog boxInt= new Dialog(this);
        boxInt.setContentView(R.layout.agences_interieur);
        boxInt.setTitle("Agences de l'intérieur");
        boxInt.show();

    }

    public void joindreConseiller(View view) {
        Intent intent = new Intent(this, JoindreUnConseillerActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
