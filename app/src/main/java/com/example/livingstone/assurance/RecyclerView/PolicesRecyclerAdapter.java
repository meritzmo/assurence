package com.example.livingstone.assurance.RecyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.livingstone.assurance.R;

import java.util.List;

/**
 * Created by livingstone on 19/02/18.
 */

public class PolicesRecyclerAdapter extends RecyclerView.Adapter<PolicesRecyclerAdapter.PolicesViewHolder> {

    private List<mesPolices> list;

    public PolicesRecyclerAdapter(List<mesPolices> list) {
        this.list = list;
    }

    @Override
    public PolicesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View convertView;
        convertView=inflater.inflate(R.layout.mes_polices,parent,false);
        return new PolicesViewHolder(convertView);
    }


    @Override
    public void onBindViewHolder(PolicesViewHolder holder, int position) {
        mesPolices item=list.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class PolicesViewHolder extends RecyclerView.ViewHolder{

        private TextView numPolice;
        private TextView nom_prod;
        private TextView capital;
        private TextView prime;
        private TextView periodicite;
        private TextView valeurpm;
        private TextView impaye;

        public PolicesViewHolder(View itemView) {
            super(itemView);
            this.numPolice =(TextView)itemView.findViewById(R.id.No_pol);
            this.nom_prod = (TextView)itemView.findViewById(R.id.prod_pol_val);
            this.capital = (TextView)itemView.findViewById(R.id.cap_pol_val);
            this.prime = (TextView)itemView.findViewById(R.id.prime_pol_val);
            this.periodicite = (TextView)itemView.findViewById(R.id.periodicite_pol_val);;
            this.valeurpm = (TextView)itemView.findViewById(R.id.valeurpm_pol_val);
            this.impaye = (TextView)itemView.findViewById(R.id.impaye);
        }

        public void bind(mesPolices m_police){
            numPolice.setText(m_police.getNumero());
            nom_prod.setText(m_police.getProduit());
            capital.setText(m_police.getCapital());
            prime.setText(m_police.getPrime());
            periodicite.setText(m_police.getPeriodicite());
            valeurpm.setText(m_police.getValeurpm());
            impaye.setText(m_police.getImpaye());
        }
    }

    public static class mesPolices{

        private String numero;
        private String produit;
        private String capital;
        private String prime;
        private String periodicite;
        private String valeurpm;
        private String impaye;

        public mesPolices(String numero, String produit, String capital, String prime, String periodicite, String valeurpm, String impaye) {
            this.numero = numero;
            this.produit = produit;
            this.capital = capital;
            this.prime = prime;
            this.periodicite = periodicite;
            this.valeurpm = valeurpm;
            this.impaye = impaye;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }

        public String getProduit() {
            return produit;
        }

        public void setProduit(String produit) {
            this.produit = produit;
        }

        public String getCapital() {
            return capital;
        }

        public void setCapital(String capital) {
            this.capital = capital;
        }

        public String getPrime() {
            return prime;
        }

        public void setPrime(String prime) {
            this.prime = prime;
        }

        public String getPeriodicite() {
            return periodicite;
        }

        public void setPeriodicite(String periodicite) {
            this.periodicite = periodicite;
        }

        public String getValeurpm() {
            return valeurpm;
        }

        public void setValeurpm(String valeurpm) {
            this.valeurpm = valeurpm;
        }

        public String getImpaye() {
            return impaye;
        }

        public void setImpaye(String impaye) {
            this.impaye = impaye;
        }
    }

}
