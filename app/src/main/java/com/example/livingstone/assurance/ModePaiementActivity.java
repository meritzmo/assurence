package com.example.livingstone.assurance;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ModePaiementActivity extends AppCompatActivity {

    private EditText editBanque;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_paiement);



        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void paiementBanque(View view) {
        Dialog boxBanque= new Dialog(this);
        boxBanque.setContentView(R.layout.paiement_banque);
        boxBanque.setTitle("Paiement par Banque");
        boxBanque.show();
        editBanque=(EditText)boxBanque.findViewById(R.id.edit_banque);
    }

    public void paiementMicrofinance(View view) {
        Dialog boxMicro= new Dialog(this);
        boxMicro.setContentView(R.layout.paiement_microfinance);
        boxMicro.setTitle("Paiement par Microfinance");
        boxMicro.show();
    }

    public void paiementMobile(View view) {
        Dialog boxMobile= new Dialog(this);
        boxMobile.setContentView(R.layout.paiement_mobile_money);
        boxMobile.setTitle("Paiement par Mobile Money");
        boxMobile.show();
    }

    public void onBOAClick(View view) {

        String editBanque_val=editBanque.getText().toString();

        if (!editBanque_val.isEmpty()){

            TextView t=new TextView(this);
            t.setText(editBanque_val);
            t.setTextColor(getResources().getColor(R.color.colorPrimary));

            TextView bank=new TextView(this);
            bank.setText("BOA");
            bank.setTextColor(getResources().getColor(R.color.colorPrimary));

            android.support.v7.app.AlertDialog.Builder ad= new android.support.v7.app.AlertDialog.Builder(this);
            ad.setTitle("Reussite: No "+t.getText().toString());
            ad.setMessage("Le numero a été enregistré avec Succès! La compagnie prendra attache avec la " +bank.getText().toString()+
                    " pour la validation du dossier physique");
            ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            ad.show();
            editBanque.setText("");

        }

    }

    public void onBtciClick(View view) {

        String editBanque_val=editBanque.getText().toString();

        if (!editBanque_val.isEmpty()){

            TextView t=new TextView(this);
            t.setText(editBanque_val);
            t.setTextColor(getResources().getColor(R.color.colorPrimary));

            TextView bank=new TextView(this);
            bank.setText("BTCI");
            bank.setTextColor(getResources().getColor(R.color.colorPrimary));

            android.support.v7.app.AlertDialog.Builder ad= new android.support.v7.app.AlertDialog.Builder(this);
            ad.setTitle("Reussite: No "+t.getText().toString());
            ad.setMessage("Le numero a été enregistré avec Succès! La compagnie prendra attache avec la " +bank.getText().toString()+
                    " pour la validation du dossier physique");
            ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            ad.show();
            editBanque.setText("");

        }

    }

    public void onBpecClick(View view) {
        String editBanque_val=editBanque.getText().toString();

        if (!editBanque_val.isEmpty()){

            TextView t=new TextView(this);
            t.setText(editBanque_val);
            t.setTextColor(getResources().getColor(R.color.colorPrimary));

            TextView bank=new TextView(this);
            bank.setText("BPEC");
            bank.setTextColor(getResources().getColor(R.color.colorPrimary));

            android.support.v7.app.AlertDialog.Builder ad= new android.support.v7.app.AlertDialog.Builder(this);
            ad.setTitle("Reussite: No "+t.getText().toString());
            ad.setMessage("Le numero a été enregistré avec Succès! La compagnie prendra attache avec la " +bank.getText().toString()+
                    " pour la validation du dossier physique");
            ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            ad.show();
            editBanque.setText("");

        }

    }

    public void onEcoClick(View view) {
        String editBanque_val=editBanque.getText().toString();

        if (!editBanque_val.isEmpty()){

            TextView t=new TextView(this);
            t.setText(editBanque_val);
            t.setTextColor(getResources().getColor(R.color.colorPrimary));

            TextView bank=new TextView(this);
            bank.setText("ECOBANK");
            bank.setTextColor(getResources().getColor(R.color.colorPrimary));

            android.support.v7.app.AlertDialog.Builder ad= new android.support.v7.app.AlertDialog.Builder(this);
            ad.setTitle("Reussite: No "+t.getText().toString());
            ad.setMessage("Le numero a été enregistré avec Succès! La compagnie prendra attache avec la " +bank.getText().toString()+
                    " pour la validation du dossier physique");
            ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            ad.show();
            editBanque.setText("");

        }

    }

    public void onDiamondClick(View view) {
        String editBanque_val=editBanque.getText().toString();

        if (!editBanque_val.isEmpty()){

            TextView t=new TextView(this);
            t.setText(editBanque_val);
            t.setTextColor(getResources().getColor(R.color.colorPrimary));

            TextView bank=new TextView(this);
            bank.setText("Diamond Bank");
            bank.setTextColor(getResources().getColor(R.color.colorPrimary));

            android.support.v7.app.AlertDialog.Builder ad= new android.support.v7.app.AlertDialog.Builder(this);
            ad.setTitle("Reussite: No "+t.getText().toString());
            ad.setMessage("Le numero a été enregistré avec Succès! La compagnie prendra attache avec la " +bank.getText().toString()+
                    " pour la validation du dossier physique");
            ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            ad.show();
            editBanque.setText("");

        }

    }
}

