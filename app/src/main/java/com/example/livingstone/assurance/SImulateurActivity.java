package com.example.livingstone.assurance;


import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.livingstone.assurance.RecyclerView.ProduitsCorrespondantsAdapter;
import com.example.livingstone.assurance.RecyclerView.ProduitsRecyclerAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;



public class SImulateurActivity extends AppCompatActivity {
    private Spinner visa_etudes,assistance;

    private RecyclerView recyclerView;
    private TextView lib_prod,prime_mens, prim_trim,prim_sem,prim_an;
    private Boolean correct_prod;
    private View mon_card;
    private EditText label;
    Calendar myCalendar = Calendar.getInstance();

    private List<ProduitsCorrespondantsAdapter.ProduitsCorr> m_produit= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulateur);

        label=(EditText) findViewById(R.id.date_nais);

        //Spinner pour Situation matrimoniale
        Spinner spinner_sit = (Spinner) findViewById(R.id.spin_sit_matrim);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter_sit= ArrayAdapter.createFromResource(this,
                R.array.sit_matrim, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_sit.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_sit.setAdapter(adapter_sit);

        //Spinner pour Civilité
        Spinner spinner_civ = (Spinner) findViewById(R.id.spin_civil);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter_civ = ArrayAdapter.createFromResource(this,
                R.array.civilite, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_civ.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_civ.setAdapter(adapter_civ);

        //Spinner pour Produit
        Spinner spinner_prod = (Spinner) findViewById(R.id.spin_produit);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter_prod = ArrayAdapter.createFromResource(this,
                R.array.produit, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_prod.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_prod.setAdapter(adapter_prod);

        //Spinner pour Periodicite
        Spinner spinner_period= (Spinner) findViewById(R.id.spin_periodiciT);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter_period = ArrayAdapter.createFromResource(this,
                R.array.periodicite, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_period.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_period.setAdapter(adapter_period);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {

            myCalendar.set(Calendar.YEAR,year);
            myCalendar.set(Calendar.MONTH,month);
            myCalendar.set(Calendar.DAY_OF_MONTH,day);

            String myFormat="dd/MM/yyyy";
            SimpleDateFormat sdf=new SimpleDateFormat(myFormat, Locale.FRANCE);
            label.setText(sdf.format(myCalendar.getTime()));
        }
    };

    public void selectionDate(View view) {
        new DatePickerDialog(this,date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onSexeClicked(View view) {

        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_masc:
                if (checked)
                    // masc
                    break;
            case R.id.radio_fem:
                if (checked)
                    // fem
                    break;
        }
    }

    public void simuler(View view) {

      /*  EditText salaire=(EditText)findViewById(R.id.salaire);
        String salaire_string=salaire.getText().toString();


        if (!salaire_string.isEmpty()){

            Double salaire_val=Double.parseDouble(salaire_string);

            prime_mens=(TextView)findViewById(R.id.mensuel_val);
            prim_trim=(TextView)findViewById(R.id.trimestriel_val);
            prim_sem=(TextView)findViewById(R.id.semestriel_val);
            prim_an=(TextView)findViewById(R.id.annuel_val);
            mon_card=findViewById(R.id.mon_card);

            prime_mens.setText(""+salaire_val*0.08);
            prim_trim.setText(""+salaire_val*0.08*3);
            prim_sem.setText(""+salaire_val*0.08*6);
            prim_an.setText(""+salaire_val*0.08*12);
            mon_card.setVisibility(View.VISIBLE);


           ajouterProduits(salaire_val);
            recyclerView = (RecyclerView) findViewById(R.id.recyclerview1);
            recyclerView.setLayoutManager(new GridLayoutManager(this,2));
            recyclerView.setAdapter(new ProduitsCorrespondantsAdapter(m_produit));
        }*/

    }

   /* private void ajouterProduits(Float salaire) {

        m_produit.clear();

        m_produit.add(new ProduitsCorrespondantsAdapter.ProduitsCorr(
                "Visa etude",
                ""+(salaire*0.05),
                ""+(salaire*0.05)*3,
                ""+(salaire*0.05)*6,
                ""+(salaire*0.05)*12));

        m_produit.add(new ProduitsCorrespondantsAdapter.ProduitsCorr(
                "Voyage plus",
                ""+(salaire*0.03),
                ""+(salaire*0.05)*3,
                ""+(salaire*0.05)*6,
                ""+(salaire*0.05)*12));

        m_produit.add(new ProduitsCorrespondantsAdapter.ProduitsCorr(
                "Assistance frais funérailles",
                ""+(salaire*0.05),
                ""+(salaire*0.05)*3,
                ""+(salaire*0.05)*6,
                ""+(salaire*0.05)*12));

        m_produit.add(new ProduitsCorrespondantsAdapter.ProduitsCorr(
                "Décès emprunteur",
                ""+(salaire*0.05),
                ""+(salaire*0.05)*3,
                ""+(salaire*0.05)*6,
                ""+(salaire*0.05)*12));

        m_produit.add(new ProduitsCorrespondantsAdapter.ProduitsCorr(
                "Supermixte",
                ""+(salaire*0.05),
                ""+(salaire*0.05)*3,
                ""+(salaire*0.05)*6,
                ""+(salaire*0.05)*12));

    }*/

}
