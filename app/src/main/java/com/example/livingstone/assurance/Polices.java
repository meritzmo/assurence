package com.example.livingstone.assurance;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

/**
 * Created by livingstone on 19/02/18.
 */

public class Polices implements Parcelable {


    private String num_police;
    private String nomProduit;
    private String capital;
    private String prime;
    private String periodicite;
    private String valeurPm;
    private String dateRegl;
    private String montantRegl;
    private String dateEffet;
    private String impaye;

    public Polices(String num_police, String nomProduit, String capital, String prime, String periodicite, String valeurPm, String dateRegl, String montantRegl, String dateEffet,String impaye) {
        this.num_police = num_police;
        this.nomProduit = nomProduit;
        this.capital = capital;
        this.prime = prime;
        this.periodicite = periodicite;
        this.valeurPm = valeurPm;
        this.dateRegl = dateRegl;
        this.montantRegl = montantRegl;
        this.dateEffet = dateEffet;
        this.impaye = impaye;
    }

    protected Polices(Parcel in) {
        num_police = in.readString();
        nomProduit = in.readString();
        capital = in.readString();
        prime = in.readString();
        periodicite = in.readString();
        valeurPm = in.readString();
        dateRegl = in.readString();
        montantRegl = in.readString();
        dateEffet = in.readString();
        impaye = in.readString();
    }

    public Polices(JSONObject police) {
        this.num_police = police.optString("num_police");
        this.nomProduit = police.optString("nom_produit");
        this.capital = police.optString("capital");
        this.prime = police.optString("prime");
        this.periodicite = police.optString("periodicite");
        this.valeurPm = police.optString("valeur_pm");
        this.dateRegl = police.optString("date_regl");
        this.montantRegl = police.optString("montant_regl");
        this.dateEffet = police.optString("date_effet");
        this.impaye = police.optString("impaye");
    }

    public String getNum_police() {
        return num_police;
    }

    public void setNum_police(String num_police) {
        this.num_police = num_police;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getPrime() {
        return prime;
    }

    public void setPrime(String prime) {
        this.prime = prime;
    }

    public String getPeriodicite() {
        return periodicite;
    }

    public void setPeriodicite(String periodicite) {
        this.periodicite = periodicite;
    }

    public String getValeurPm() {
        return valeurPm;
    }

    public void setValeurPm(String valeurPm) {
        this.valeurPm = valeurPm;
    }

    public String getDateRegl() {
        return dateRegl;
    }

    public void setDateRegl(String dateRegl) {
        this.dateRegl = dateRegl;
    }

    public String getMontantRegl() {
        return montantRegl;
    }

    public void setMontantRegl(String montantRegl) {
        this.montantRegl = montantRegl;
    }

    public String getDateEffet() {
        return dateEffet;
    }

    public void setDateEffet(String dateEffet) {
        this.dateEffet = dateEffet;
    }

    public String getImpaye() {
        return impaye;
    }

    public void setImpaye(String impaye) {
        this.impaye = impaye;
    }

    public static final Creator<Polices> CREATOR = new Creator<Polices>() {
        @Override
        public Polices createFromParcel(Parcel in) {
            return new Polices(in);
        }

        @Override
        public Polices[] newArray(int size) {
            return new Polices[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(num_police);
        parcel.writeString(nomProduit);
        parcel.writeString(capital);
        parcel.writeString(prime);
        parcel.writeString(periodicite);
        parcel.writeString(valeurPm);
        parcel.writeString(dateRegl);
        parcel.writeString(montantRegl);
        parcel.writeString(dateEffet);
        parcel.writeString(impaye);
    }
}
