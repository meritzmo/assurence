package com.example.livingstone.assurance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void design1(View view) {
        Intent intent = new Intent(this, Design1Activity.class);
        startActivity(intent);
    }

    public void design2(View view) {
        Intent intent = new Intent(this, Design2Activity.class);
        startActivity(intent);
    }

    public void design3(View view) {
        Intent intent = new Intent(this, Design3Activity.class);
        startActivity(intent);
    }

    public void design4(View view) {

        Intent intent = new Intent(this, Design4Activity.class);
        startActivity(intent);

    }

    public void design5(View view) {
        Intent intent = new Intent(this, Design5Activity.class);
        startActivity(intent);
    }
}
