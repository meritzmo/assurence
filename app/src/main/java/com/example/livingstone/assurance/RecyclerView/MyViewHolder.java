package com.example.livingstone.assurance.RecyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.livingstone.assurance.R;

/**
 * Created by livingstone on 13/02/18.
 */

public class MyViewHolder extends RecyclerView.ViewHolder {

        private  TextView nom;
        private  TextView description;
        private  TextView periodicite;
        private  TextView plus;

        public MyViewHolder(View itemView) {
            super (itemView);

            nom=(TextView)itemView.findViewById(R.id.nom_produit);
            description=(TextView)itemView.findViewById(R.id.desc_produit);
            periodicite=(TextView)itemView.findViewById(R.id.periodicite);
            plus=(TextView)itemView.findViewById(R.id.plus);
        }

        // remplir la cellule avec un oblet mesproduits

        public void bind (ProduitsRecyclerAdapter.MesProduits m_produit){
            nom.setText(m_produit.getNom());
            description.setText(m_produit.getDescription());
            periodicite.setText(m_produit.getPeriodicite());
            plus.setText(m_produit.getPlus());
        }

}
