package com.example.livingstone.assurance.monCompte;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.livingstone.assurance.R;

public class MonCompteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mon_compte);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void notifications(View view) {
        Intent intent = new Intent(this, NotificationsActivity.class);
        startActivity(intent);
    }

    public void estimationRachat(View view) {
        Intent intent = new Intent(this, EstimationRachatActivity.class);
        startActivity(intent);
    }

    public void declarationSinistre(View view) {
        Intent intent = new Intent(this, DeclarationSInistreActivity.class);
        startActivity(intent);
    }

    public void mesPolices(View view) {
        Intent intent = new Intent(this, MesPolicesActivity.class);
        startActivity(intent);
    }


    public void mesSimilat(View view) {
        Intent intent = new Intent(this, MesSimilatActivity.class);
        startActivity(intent);
    }
}
