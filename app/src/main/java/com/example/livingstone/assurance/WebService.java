package com.example.livingstone.assurance;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by livingstone on 19/02/18.
 */

public class WebService {

    private WebServiceListener wl;
    private Context context;

    String url_base="http://10.42.0.1";

    String url_base2="http://app.landogroupcie.com/mba/api";



    public WebService(Context cont)
    {
        this.context = cont;
        this.wl=(WebServiceListener)cont;
    }


    public String getPolice() {
        try{

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url_base+"/assur_polices.php")
                    .build();
            Response response = client.newCall(request).execute();

            final String responseData = response.body().string();
            Log.d("resp",responseData);
            return responseData;
        }

        catch (Exception ex)
        {
            try{
                OkHttpClient client2 = new OkHttpClient();
                Request request2 = new Request.Builder()
                        .url(url_base2+"/assur_polices.php")
                        .build();
                Response response2 = client2.newCall(request2).execute();

                final String responseData2 = response2.body().string();
                //Log.d("resp",responseData);
                return responseData2;

            } catch (Exception ex2){
                return "no_network";
                // wl.afficher(ex.getMessage() + " classe:" + ex.getClass().getName());
                // wl.afficher("Problème de connexion",context);
            }


        }
    }

    public String getPoliceRachat(String police) {
        try{

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url_base+"/police_rachat.php?police="+police)
                    .build();
            Response response = client.newCall(request).execute();



            final String responseData = response.body().string();
            Log.d("resp",responseData);
            if (response==null) return "aucun";
            return responseData;
        }

        catch (Exception ex)
        {
            try{
                OkHttpClient client2 = new OkHttpClient();
                Request request2 = new Request.Builder()
                        .url(url_base2+"/police_rachat.php?police="+police)
                        .build();
                Response response2 = client2.newCall(request2).execute();

                final String responseData2 = response2.body().string();
                //Log.d("resp",responseData);
                return responseData2;

            } catch (Exception ex2){
                return "no_network";
                // wl.afficher(ex.getMessage() + " classe:" + ex.getClass().getName());
                // wl.afficher("Problème de connexion",context);
            }

        }
    }

    public static List<Polices> parse(final String json_recup) {
        try {
            final List<Polices> trajets = new ArrayList<>();
            final JSONArray jTrajetsArray = new JSONArray(json_recup);

            for (int i = 0; i < jTrajetsArray.length(); i++) {
                trajets.add(new Polices(jTrajetsArray.optJSONObject(i)));
            }
            return trajets;
        } catch (JSONException e) {
            // Log.v(TAG, "[JSONException] e : " + e.getMessage());
        }
        return null;
    }


    public interface WebServiceListener
    {
        public void afficher(String err);
    }
}
