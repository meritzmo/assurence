package com.example.livingstone.assurance.RecyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.livingstone.assurance.R;

import java.util.List;

/**
 * Created by livingstone on 16/02/18.
 */

public class ProduitsCorrespondantsAdapter extends RecyclerView.Adapter<ProduitsCorrespondantsViewHolder> {

    private List<ProduitsCorr> list;

    public ProduitsCorrespondantsAdapter(List<ProduitsCorr> list) {

        this.list = list;
    }


    @Override
    public ProduitsCorrespondantsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View convertView;
        convertView=inflater.inflate(R.layout.produits_du_salaire,parent,false);

        return new ProduitsCorrespondantsViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(ProduitsCorrespondantsViewHolder holder, int position) {
        ProduitsCorr item=list.get(position);
        holder.bind(item);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ProduitsCorr{

        private  String nom_produit;
        private  String mensuel;
        private  String trimestriel;
        private  String semestriel;
        private  String annuel;

        public ProduitsCorr(String nom_produit, String mensuel, String trimestriel, String semestriel, String annuel) {
            this.nom_produit = nom_produit;
            this.mensuel = mensuel;
            this.trimestriel = trimestriel;
            this.semestriel = semestriel;
            this.annuel = annuel;
        }

        public String getNom_produit() {
            return nom_produit;
        }

        public void setNom_produit(String nom_produit) {
            this.nom_produit = nom_produit;
        }

        public String getMensuel() {
            return mensuel;
        }

        public void setMensuel(String mensuel) {
            this.mensuel = mensuel;
        }

        public String getTrimestriel() {
            return trimestriel;
        }

        public void setTrimestriel(String trimestriel) {
            this.trimestriel = trimestriel;
        }

        public String getSemestriel() {
            return semestriel;
        }

        public void setSemestriel(String semestriel) {
            this.semestriel = semestriel;
        }

        public String getAnnuel() {
            return annuel;
        }

        public void setAnnuel(String annuel) {
            this.annuel = annuel;
        }
    }
}
