package com.example.livingstone.assurance.RecyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.livingstone.assurance.R;

/**
 * Created by livingstone on 16/02/18.
 */

public class ProduitsCorrespondantsViewHolder extends RecyclerView.ViewHolder{

    private TextView nom_produit;
    private  TextView mensuel;
    private  TextView trimestriel;
    private  TextView semestriel;
    private  TextView annuel;

    public ProduitsCorrespondantsViewHolder(View itemView) {
        super(itemView);

        this.nom_produit = (TextView)itemView.findViewById(R.id.produit_corresp);
        this.mensuel = (TextView)itemView.findViewById(R.id.mensuel_val);
        this.trimestriel = (TextView)itemView.findViewById(R.id.trimestriel_val);
        this.semestriel = (TextView)itemView.findViewById(R.id.semestriel_val);
        this.annuel = (TextView)itemView.findViewById(R.id.annuel_val);
    }

    // remplir la cellule avec un objet produitscorrespondants

   public void bind (ProduitsCorrespondantsAdapter.ProduitsCorr m_produit){
        nom_produit.setText(m_produit.getNom_produit());
        mensuel.setText(m_produit.getMensuel());
        trimestriel.setText(m_produit.getTrimestriel());
        semestriel.setText(m_produit.getSemestriel());
        semestriel.setText(m_produit.getSemestriel());
        annuel.setText(m_produit.getAnnuel());
    }

}
