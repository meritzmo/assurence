package com.example.livingstone.assurance.RecyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.livingstone.assurance.R;

import java.util.List;

/**
 * Created by livingstone on 13/02/18.
 */

public class ProduitsRecyclerAdapter extends RecyclerView.Adapter<MyViewHolder>{

    private List<MesProduits> list;

    public ProduitsRecyclerAdapter(List<MesProduits> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View convertView;
        convertView=inflater.inflate(R.layout.activity_produits,parent,false);
        return new MyViewHolder(convertView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MesProduits item=list.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MesProduits{
        private String nom;
        private String description;
        private String periodicite;
        private String plus;

        public MesProduits(String nom, String description, String periodicite,String plus) {
            this.nom = nom;
            this.description = description;
            this.periodicite = periodicite;
            this.plus = plus;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPeriodicite() {
            return periodicite;
        }

        public void setPeriodicite(String periodicite) {
            this.periodicite = periodicite;
        }

        public String getPlus() {
            return plus;
        }

        public void setPlus(String plus) {
            this.plus = plus;
        }
    }
}
