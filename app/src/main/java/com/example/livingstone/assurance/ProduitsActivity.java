package com.example.livingstone.assurance;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.livingstone.assurance.RecyclerView.CustomRecyclerViewItemClickListener;
import com.example.livingstone.assurance.RecyclerView.ProduitsRecyclerAdapter;
import com.example.livingstone.assurance.RecyclerView.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class ProduitsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private List <ProduitsRecyclerAdapter.MesProduits> m_produit= new ArrayList<>();

    Dialog boxDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mon_recycler_view);


        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        ajouterProduits();

        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new ProduitsRecyclerAdapter (m_produit));

        recyclerView.addOnItemTouchListener(new CustomRecyclerViewItemClickListener(this, new RecyclerItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                TextView nom=(TextView)v.findViewById(R.id.nom_produit);
                TextView desc=(TextView)v.findViewById(R.id.desc_produit);
                TextView period=(TextView)v.findViewById(R.id.periodicite);

                boxDetails = new Dialog(ProduitsActivity.this);
                boxDetails.setContentView(R.layout.details_produits);
                TextView affiche_desc=(TextView)boxDetails.findViewById(R.id.affiche_desc);
                TextView affiche_period=(TextView)boxDetails.findViewById(R.id.affiche_period);
                affiche_desc.setText(desc.getText().toString());
                affiche_period.setText(period.getText().toString());
                boxDetails.setTitle(nom.getText().toString());
                boxDetails.show();

            }

            @Override
            public void onLongClick(View v, int position) {

            }
        }));

    }

    public void dismiss(View view) {
        boxDetails.dismiss();
    }

    private void ajouterProduits() {

        m_produit.add(new ProduitsRecyclerAdapter.MesProduits(
                "Visa Etudes",
                "Ce produit garanti aux élèves une assistance ",
                "Périodicité: Mensuel|Trimestriel|Annuel",
                "Plus..."));

        m_produit.add(new ProduitsRecyclerAdapter.MesProduits(
                "Assistance frais funéraille",
                "Ce produit garanti aux clients une assistance en cas de..",
                "Périodicité: Mensuel|Trimestriel|Annuel",
                "Plus..."));


        m_produit.add(new ProduitsRecyclerAdapter.MesProduits(
                "Décès emprunteur",
                "Ce produit garanti aux clients une assistance...",
                "Périodicité: Mensuel|Trimestriel|Annuel",
                "Plus..."));


        m_produit.add(new ProduitsRecyclerAdapter.MesProduits(
                "Supermixte",
                "Ce produit garanti aux clients une assistance...",
                "Périodicité: Mensuel|Trimestriel|Annuel",
                "Plus..."));

        m_produit.add(new ProduitsRecyclerAdapter.MesProduits(
                "Voyage plus",
                "Ce produit garanti aux clients une assistance en cas de voyage",
                "Périodicité: Mensuel|Trimestriel|Annuel",
                "Plus..."));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
