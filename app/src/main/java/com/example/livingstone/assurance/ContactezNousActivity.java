package com.example.livingstone.assurance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

public class ContactezNousActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactez_nous);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    public void nosAgences(View view) {
        Intent intent = new Intent(this, NosAgencesActivity.class);
        startActivity(intent);
    }

    public void joindreConseiller(View view) {
        Intent intent = new Intent(this, JoindreUnConseillerActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
