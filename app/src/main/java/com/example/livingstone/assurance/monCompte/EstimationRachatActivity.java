package com.example.livingstone.assurance.monCompte;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.livingstone.assurance.Polices;
import com.example.livingstone.assurance.R;
import com.example.livingstone.assurance.WebService;

import java.util.ArrayList;
import java.util.List;

public class EstimationRachatActivity extends AppCompatActivity implements WebService.WebServiceListener{

    private View mon_card;
    private View monLayout;
    ProgressBar mPgBar;
    String retour="";
    TextView num_pol,nom_prod,cap_pol,prime_pol,period_pol,valeur_pm,impaye,estimation;
    List<Polices> polices_ws = new ArrayList<>();
    Boolean network;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estimation_rachat);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }



         num_pol=(TextView) findViewById(R.id.No_pol);
         nom_prod=(TextView) findViewById(R.id.prod_pol_val);
         cap_pol=(TextView) findViewById(R.id.cap_pol_val);
         prime_pol=(TextView) findViewById(R.id.prime_pol_val);
         period_pol=(TextView) findViewById(R.id.periodicite_pol_val);
         valeur_pm=(TextView) findViewById(R.id.valeurpm_pol_val);
         impaye=(TextView) findViewById(R.id.impaye);
         estimation=(TextView) findViewById(R.id.est_res);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void valider(View view) {

        String police = ((EditText)findViewById(R.id.police)).getText().toString();
        mPgBar=findViewById(R.id.pgbar);

        if (!police.isEmpty()){

            mPgBar.setVisibility(View.VISIBLE);
            RachatTask rachatTask=new RachatTask(police);
            rachatTask.execute();

          /*  num_pol.setText(polices_ws.get(0).getNum_police());
            nom_prod.setText(polices_ws.get(0).getNomProduit());
            cap_pol.setText(polices_ws.get(0).getCapital());
            prime_pol.setText(polices_ws.get(0).getPrime());
            period_pol.setText(polices_ws.get(0).getPeriodicite());
            valeur_pm.setText(polices_ws.get(0).getValeurPm());
            impaye.setText(polices_ws.get(0).getImpaye());
            estimation.setText(""+(Float.parseFloat(polices_ws.get(0).getValeurPm())-Float.parseFloat(polices_ws.get(0).getCapital())*0.1));
       */
        }



       /* TextView t=(TextView) findViewById(R.id.No_pol);

        if (!num_val.isEmpty()){

            mon_card=findViewById(R.id.mon_card);
            monLayout=findViewById(R.id.monlayout);
            mon_card.setVisibility(View.VISIBLE);
            monLayout.setVisibility(View.VISIBLE);
      }*/
    }

    @Override
    public void afficher(String err) {
        android.support.v7.app.AlertDialog.Builder ad= new android.support.v7.app.AlertDialog.Builder(this);
        ad.setTitle("Erreur");
        ad.setMessage(err);
        ad.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ad.show();
    }


    public class RachatTask extends AsyncTask<String,Void,List <Polices>>{

        private String pol;

        private List<Polices> maliste;

        public RachatTask(String pol) {
            this.pol = pol;
        }

        @Override
        protected List<Polices> doInBackground(String... strings) {


            WebService ws = new WebService(EstimationRachatActivity.this);
            String data=ws.getPoliceRachat(pol);

            if(!data.equals("no_network") )
            {
                network =true;

                if(!data.equals("aucun")){
                    maliste = WebService.parse(data);

                    if ((maliste!=null) && (maliste.size() != 0) ) {

                        retour="not_null";

                        return maliste;
                    }

            } else retour="aucun";

            }else if (data.equals("no_network"))   network =false;

            return null;
        }

        @Override
        protected void onPostExecute(List<Polices> list) {
            super.onPostExecute(list);

            Log.d("retour",retour);
            if(network){
                mPgBar.setVisibility(View.GONE);
                if (retour.equals("not_null")){

                    num_pol.setText(list.get(0).getNum_police());
                    nom_prod.setText(list.get(0).getNomProduit());
                    cap_pol.setText(list.get(0).getCapital());
                    prime_pol.setText(list.get(0).getPrime());
                    period_pol.setText(list.get(0).getPeriodicite());
                    valeur_pm.setText(list.get(0).getValeurPm());
                    impaye.setText(list.get(0).getImpaye());
                    estimation.setText(""+(Float.parseFloat(list.get(0).getValeurPm())-Float.parseFloat(list.get(0).getCapital())*0.1));

                    mon_card=findViewById(R.id.mon_card);
                    monLayout=findViewById(R.id.monlayout);
                    mon_card.setVisibility(View.VISIBLE);
                    monLayout.setVisibility(View.VISIBLE);


                } else{
                    afficher("Aucune police correspondante au numero saisit");
                }

            } else {
                mPgBar.setVisibility(View.GONE);
                afficher("Problème de connexion au serveur");
            }

        }

    }
}
